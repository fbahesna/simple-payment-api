<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\payments;
use Faker\Generator as Faker;

$factory->define(payments::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'nametest660@gmail.com',
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'created_at' => now(),
        'updated_at' => now()
    ];
});
