<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\PaymentFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(PaymentFactory::class,10)->make();
            // How many genres you need, defaulting to 10
            $count = (int)$this->command->ask('How many payment do you need ?', 10);

            $this->command->info("Creating {$count} payment.");
    
            // Create the Genre
            $genres = factory(App\payments::class, $count)->create();
    
            $this->command->info('Genres Created!');
    }
}
