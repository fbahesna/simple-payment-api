<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{

    use RefreshDatabase;
 
    /**
     * A basic test example.
     *
     * @return void
     */
   
    public function testGetPayment()
    {
        $response = $this->get(route('get.payment'));

        $response->assertStatus(200);
    }

    public function testCreatePayment()
    {
        $response = $this->json('POST', route('create.payment'), [
            'name' => 'Sally' ,
            'email' => 'nametest660@gmail.com',
            'password' => 'Kamu12341234'
        ]);

        $response
            ->assertStatus(200);
    }

    public function testEditPayment()
    {
        $response = $this->json('PUT', route('edit.payment',["id" => 1]), [
            'name' => 'Sally Sandiri' ,
            'email' => 'nametest660@gmail.com',
        ]);

        $response
            ->assertStatus(200);
    }

    public function testDeletePayment()
    {
        $response = $this->json('delete', route('delete.payment', ["id" => 1]), [
            'name' => 'Sally Sandiri' ,
            'email' => 'nametest660@gmail.com',
        ]);

        $response
            ->assertStatus(200);
    }

    public function testActivatePayment()
    {   
        try {
            $response = $this->json('PATCH', '/api/payments/2/activate');
        
            $response
                ->assertStatus(200);
        } catch (\Exception $th) {
            return $th;
        }

    }

    public function testDeactivatePayment()
    {
        try {
        $response = $this->json('PATCH','/api/payments/2/deactivate');
   
        $response
            ->assertStatus(200);
        } catch (\Exception $th) {
            return $th;
        }
    }
}
