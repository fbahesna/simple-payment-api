<?php

namespace App\Http\Controllers;
use App\payments as payment;
use Hash;
use Exception;
use DB;
use Mail;

use Illuminate\Http\Request;

class paymentController extends Controller
{
    
    public function getPayment()
    {
        $data = payment::paginate(5);
        return response()->json([
            "status" => 200 , "data" => $data
        ]);
    }

    public function createPayment(Request $request)
    {
        $validator = $request->validate([
            'name'  => 'required',
            'email'      => 'required|email',
            'password'     => 'required'
        ]);

        if($validator){
            $password = $request->input('password');
            try {
                DB::beginTransaction();
                $data = payment::create([
                    "name" => $request->input('name'),
                    "email" => $request->input('email'),
                    "password" => Hash::make($password),
                    "created_at" => now()
                ]);
    
                DB::commit();
                return response()->json([
                    "status" => 201 , "message" => "New Payment Created!" 
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    "status" => 401 , "message" => $e 
                ]);
            }
        }
    }

    public function editPayment(Request $request , $id)
    {
        try {
            $request->validate([
                'name'  => 'required',
                'email'      => 'required|email'
            ]);
                    
            DB::beginTransaction();
            $payment = payment::find($id);
            $payment->name = $request->input('name');
            $payment->email = $request->input('email');
            $payment->save();

            DB::commit();
            return response()->json([
                "status" => 200 , "message" => "Payment Updated!"
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                "status" => 400 , "message" => $e
            ]);
        }
    }

    public function deletePayment($id)
    {
        $payment = payment::find($id);
        if($payment){
            $payment->delete();
    
            return response()->json([
                "status" => 200 , "message" => "Payment Deleted!"
            ]);
        }else{
            return response()->json([
                "status" => 401 , "message" => "Error, id not found!"
            ]);
        }
    }

    public function activatePayment($id)
    {
        $payment = payment::find($id);
        //cek if the payment already activated
        if($payment->is_active != 0){
            return response()->json([
                "status" => 201 , "message" => "Payment " .$id." Already Activated"
            ]);
        }
        //cek the id of the payment
        if($payment != null){
            $a = DB::table('payments')->where('id',$id)
                ->update(['is_active' => 1]);
                $data = [
                    'title' => 'Email Payment Confirmation',
                    'user_name' =>  $payment->name,
                    'content' => 'Thank you your payment has been activated'
                ];
                  Mail::send('email', $data, function ($m) use ($payment) {
                    $m->from('fbahezna@gmail.com', 'Your Application');

                    $m->to($payment->email)->subject('Your Reminder!');
                });

            return response()->json([
                "status" => 200 , "message" => "Payment " .$id." Changed! and email is sended"
            ]);
        }else{
            return response()->json([
                "status" => 401 , "message" => "Payment " .$id." Not Found! "
            ]);      
        }
    }

    
    public function deactivatePayment($id)
    {
        $payment = payment::find($id);
        //cek if the payment already activated
        if($payment->is_active != 1){
            return response()->json([
                "status" => 201 , "message" => "Payment " .$id." Is Still Deactivated! Please choose another activated payment"
            ]);
        }
        //cek the id of the payment
        if($payment != null){
            DB::table('payments')->where('id',$id)
                ->update(['is_active' => 0]);
                $data = [
                    'title' => 'Payment Deactivated',
                    'user_name' =>  $payment->name,
                    'content' => " We're Sorry, your payment has been deactivated. Please contact our Customer Services "
                ];
                  Mail::send('email', $data, function ($m) use ($payment) {
                    $m->from('fbahezna@gmail.com', 'Your Application');

                    $m->to($payment->email)->subject('Your Reminder!');
                });

            return response()->json([
                "status" => 201 , "message" => "Payment " .$id." Changed! and email is sended"
            ]);
        }else{
            return response()->json([
                "status" => 401 , "message" => "Payment " .$id." Not Found! "
            ]);      
        }
    }
}
